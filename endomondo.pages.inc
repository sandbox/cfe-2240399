<?php
/**
 * @file
 * Page callbacks for Endomondo module.
 */

/**
 * Endomondo settings form.
 */
function endomondo_admin_form($form, &$form_state) {
  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for Endomondo, you must register an application with Endomondo and add the provided keys here.'),
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('endomondo/oauth', array('absolute' => TRUE)),
  );
  $form['oauth']['endomondo_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('endomondo_consumer_key', NULL),
  );
  $form['oauth']['endomondo_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('endomondo_consumer_secret', NULL),
  );

  // Endomondo import settings.
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Endomondo Import Settings'),
    '#description' => t('Enable/disable the import of user workouts.'),
  );
  $form['import']['endomondo_import'] = array(
    '#type' => 'checkbox',
    '#title' => t('Import workouts'),
    '#description' => t('Uncheck this checkbox to completely disable imports.'),
    '#default_value' => variable_get('endomondo_import', TRUE),
  );

  // Endomondo external APIs settings.
  $form['endomondo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Endomondo Settings'),
    '#description' => t('The following settings connect Endomondo module with external APIs.'),
  );
  $form['endomondo']['endomondo_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Endomondo host'),
    '#default_value' => variable_get('endomondo_host', ENDOMONDO_HOST),
  );
  $form['endomondo']['endomondo_api'] = array(
    '#type' => 'textfield',
    '#title' => t('Endomondo API'),
    '#default_value' => variable_get('endomondo_api', ENDOMONDO_API),
  );

  return system_settings_form($form);
}

/**
 * Returns the user settings form.
 */
function endomondo_user_settings($account = NULL) {

  // Verify OAuth keys.
  if (!endomondo_api_keys()) {
    $variables = array('@endomondo-settings' => url('admin/config/services/endomondo/settings'));
    $output = '<p>' . t('You need to create an application in order to use the Endomondo API. Please fill out the OAuth fields at <a href="@endomondo-settings">Endomondo Settings</a> and then return here.', $variables) . '</p>';
  }
  else {
    module_load_include('inc', 'endomondo');
    if (!$account) {
      $endomondo_accounts = endomondo_account_load_all();
    }
    else {
      $endomondo_accounts = endomondo_endomondo_accounts($account);
    }

    $output = array();
    if (count($endomondo_accounts)) {
      // List Endomondo accounts.
      // $output['header']['#markup'] = '<p>';
      // $variables = array('@users' => url('users'));
      // $output['header']['#markup'] .= t('You can view the full list of users at the <a href="@users">Users</a> view.', $variables);
      // $output['header']['#markup'] .= '</p>';
      $output['list_form'] = drupal_get_form('endomondo_account_list_form', $endomondo_accounts);
    }
    else {
      // No accounts added. Inform about how to add one.
      $output['header'] = array(
        '#markup' => '<p>' . t('No Endomondo accounts have been added yet. Click on the following button to add one.') . '</p>',
      );
    }

    $output['add_account'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add Endomondo accounts'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    if (user_access('add authenticated endomondo accounts')) {
      $output['add_account']['form'] = drupal_get_form('endomondo_auth_account_form');
    }
  }

  return $output;
}


/**
 * Formats each Endomondo account as a row within a form.
 */
function endomondo_account_list_form($form, $form_state, $endomondo_accounts = array()) {
  $form['#tree'] = TRUE;
  $form['accounts'] = array();

  foreach ($endomondo_accounts as $endomondo_account) {
    $form['accounts'][] = _endomondo_account_list_row($endomondo_account);
  }

  if (!empty($endomondo_accounts)) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
  }

  return $form;
}

/**
 * Returns the form fields to manage a Endomondo account.
 */
function _endomondo_account_list_row($account) {
  $form['#account'] = $account;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $account->id,
  );

  $form['name'] = array(
    '#type' => 'value',
    '#value' => $account->name,
  );

  if (user_access('administer endomondo accounts')) {
    $user = user_load($account->uid);
    $form['user'] = array(
      '#markup' => l($user->name, 'user/' . $account->uid),
    );
  }

  $form['auth'] = array(
    '#markup' => $account->is_auth() ? t('Yes') : t('No'),
  );

  $form['import'] = array(
    '#type' => 'checkbox',
    '#default_value' => $account->import ? $account->import : '',
  );
  if ($account->import == TRUE) {
    $form['import']['#suffix'] = l(t('View'), 'workouts/' . $account->id, array('attributes' => array('target' => '_blank')));
  }

  $form['delete'] = array(
    '#type' => 'checkbox',
  );

  return $form;
}

/**
 * Themes the list of Endomondo accounts.
 */
function theme_endomondo_account_list_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('Name'),
    t('Endomondo ID'),
  );
  if (user_access('administer endomondo accounts')) {
    $header[] = t('Added by');
  }
  $header = array_merge($header, array(
    t('Auth'),
    t('Import'),
    t('Delete'),
  ));

  $rows = array();
  foreach (element_children($form['accounts']) as $key) {
    $element = &$form['accounts'][$key];
    $row = array(
      $element['name']['#value'],
      $element['id']['#value'],
    );
    if (user_access('administer endomondo accounts')) {
      $row[] = drupal_render($element['user']);
    }
    $row = array_merge($row, array(
      drupal_render($element['auth']),
      drupal_render($element['import']),
      drupal_render($element['delete']),
    ));
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Form submit handler for altering the list of Endomondo accounts.
 */
function endomondo_account_list_form_submit($form, &$form_state) {
  $accounts = $form_state['values']['accounts'];
  foreach ($accounts as $account) {
    if (empty($account['delete'])) {
      endomondo_account_save($account);
    }
    else {
      $endomondo_account = endomondo_account_load($account['id']);
      endomondo_account_delete($account['id']);
      drupal_set_message(t('The Endomondo account <em>!account</em> was deleted.',
        array('!account' => check_plain($endomondo_account->name))));
    }
  }
  drupal_set_message(t('The Endomondo account settings were updated.'));
}

/**
 * Form to add an authenticated Endomondo account.
 */
function endomondo_auth_account_form($form, $form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Connect your user with Endomondo'),
    '#prefix' => t('Authenticated accounts can integrate Endomondo in their profile.<br /><br />'),
  );

  return $form;
}

/**
 * Form validation for adding a new Endomondo account.
 */
function endomondo_auth_account_form_validate($form, &$form_state) {
  $key = variable_get('endomondo_consumer_key', '');
  $secret = variable_get('endomondo_consumer_secret', '');
  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your consumer key and secret key at <a href="!url">Endomondo settings</a>.',
      array(
        '!url' => url('admin/config/services/endomondo'),
      )));
  }
}

/**
 * Form submit handler for adding a Endomondo account.
 *
 * Loads Endomondo account details and adds them to the user account
 */
function endomondo_auth_account_form_submit($form, &$form_state) {
  $key = variable_get('endomondo_consumer_key', '');
  $secret = variable_get('endomondo_consumer_secret', '');
  $endomondo = new Endomondo($key, $secret);
  $token = $endomondo->get_request_token();

  if ($token) {
    $_SESSION['endomondo_oauth']['token'] = $token;
    $_SESSION['endomondo_oauth']['destination'] = $_GET['q'];
    // Check for the overlay.
    if (module_exists('overlay') && overlay_get_mode() == 'child') {
      overlay_close_dialog($endomondo->get_authorize_url($token), array('external' => TRUE));
      overlay_deliver_empty_page();
    }
    else {
      drupal_goto($endomondo->get_authorize_url($token));
    }
  }
  else {
    drupal_set_message(t('Could not obtain a valid token from the Endomondo API. Please review the configuration.'),
      'error');
  }
}

/**
 * Wrapper to call drupal_form_submit().
 */
function endomondo_oauth_callback() {
  if (isset($_GET['denied']) || empty($_GET['oauth_token'])) {
    drupal_set_message(t('The connection to Endomondo failed. Please try again.'), 'error');
    global $user;
    if ($user->uid) {
      // User is logged in, was attempting to OAuth a Endomondo account.
      drupal_goto('admin/config/services/endomondo');
    }
    else {
      // Anonymous user, redirect to front page.
      drupal_goto('<front>');
    }
  }
  $form_state['values']['oauth_token'] = $_GET['oauth_token'];
  drupal_form_submit('endomondo_oauth_callback_form', $form_state);
}

/**
 * Form builder function.
 */
function endomondo_oauth_callback_form($form, &$form_state) {
  $form['#post']['oauth_token'] = $_GET['oauth_token'];
  $form['oauth_token'] = array(
    '#type' => 'hidden',
    '#default_value' => $_GET['oauth_token'],
  );
  return $form;
}

/**
 * Validate results from Endomondo OAuth return request.
 */
function endomondo_oauth_callback_form_validate($form, &$form_state) {
  $key = variable_get('endomondo_consumer_key', '');
  $secret = variable_get('endomondo_consumer_secret', '');

  if (isset($_SESSION['endomondo_oauth'])) {
    $form_state['endomondo_oauth'] = $_SESSION['endomondo_oauth'];
    unset($_SESSION['endomondo_oauth']);
  }
  else {
    form_set_error('oauth_token', t('Invalid Endomondo OAuth request'));
  }

  if (isset($form_state['endomondo_oauth']['token'])) {
    $token = $form_state['endomondo_oauth']['token'];
    if (!is_array($token) || !$key || !$secret) {
      form_set_error('oauth_token', t('Invalid Endomondo OAuth request'));
    }
    if ($token['oauth_token'] != $form_state['values']['oauth_token']) {
      form_set_error('oauth_token', t('Invalid OAuth token.'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Endomondo OAuth request'));
  }

  module_load_include('inc', 'endomondo');

  if ($endomondo = new Endomondo($key, $secret, $token['oauth_token'], $token['oauth_token_secret'])) {
    // Collect oauth_verifier from url.
    if ($response = $endomondo->get_access_token($_GET['oauth_verifier'])) {
      $form_state['endomondo_oauth']['response'] = $response;
    }
    else {
      form_set_error('oauth_token', t('Invalid Endomondo OAuth request'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Endomondo OAuth request'));
  }
}

/**
 * Submit callback.
 *
 * Handle a Endomondo OAuth return request and
 * store the account credits in the DB.
 */
function endomondo_oauth_callback_form_submit($form, &$form_state) {
  $key = variable_get('endomondo_consumer_key', '');
  $secret = variable_get('endomondo_consumer_secret', '');
  $response = $form_state['endomondo_oauth']['response'];

  $endomondo = new Endomondo($key, $secret, $response['oauth_token'], $response['oauth_token_secret']);
  try {
    $endomondo_account = $endomondo->users_show();
  }
  catch (EndomondoException $e) {
    form_set_error('name', t('Request failed: @message.', array('@message' => $e->getMessage())));
    return;
  }
  // Save the new Endomondo account and set the user's uid who added it.
  $endomondo_account->set_auth($response);
  global $user;
  $endomondo_account->uid = $user->uid;
  endomondo_account_save($endomondo_account, TRUE);
  drupal_set_message(t('The profile <i>@name</i> is now connected with user <i>@user</i>', array('@name' => $endomondo_account->name, '@user' => $user->name)));

  $form_state['programmed'] = FALSE;
  $form_state['redirect'] = $form_state['endomondo_oauth']['destination'];
}
