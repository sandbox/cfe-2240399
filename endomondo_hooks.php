<?php

/**
 * @file
 * Sample hooks demonstrating usage in Endomondo.
 */

/**
 * Loads Endomondo accounts for a user.
 *
 * @param stdclass $account
 *   stdClass object containing a user account.
 *
 * @return stdclass
 *   array of stdClass objects with the associated Endomondo accounts.
 * @see endomondo_endomondo_accounts().
 */
function hook_endomondo_accounts($account) {
}

/**
 * Loads Endomondo accounts for a user.
 *
 * @param stdclass $account
 *   stdClass object containing a user account.
 *
 * @return array
 *   array of stdClass objects with the associated Endomondo accounts.
 * @see endomondo_endomondo_accounts()
 */
function hook_endomondo_account_save($account) {
}

/**
 * Notifies of a saved workout.
 *
 * @param stdclass $status
 *   stdClass containing information about the workout.
 */
function hook_endomondo_workout_save($status) {
}
