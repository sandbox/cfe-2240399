<?php
/**
 * @file
 * Theme file for the Endomondo user stats block
 */
?>
<div class="stats">
<?php	foreach($workouts as $total): ?>
	<div class="stat">
		<div class="stat-title"><?php print $total['title'] . ': '; ?></div>
		<div class="stat-value"><?php print round($total['value'], 2) . ' ' . $total['unit']; ?></div>
	</div>
<?php endforeach; ?>
</div>