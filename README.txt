
-- SUMMARY --

The Endomondo module allows the users of your site to connect their Drupal profile with
their Endomondo profile. The module will automatically pull in recent workouts during cron runs.
API implementation is currently limited and most people will not be able to obtain


-- REQUIREMENTS --

Oauth common:
https://drupal.org/project/oauth

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Obtain Endomondo API key and secret from Endomondo, you need to contact the company 
  in order to obtain these since their API is currently closed for most people.

* Configure user permissions in Administration � People � Permissions:

  - Administer Endomondo settings

  - Add authenticated Endomondo accounts

  - Administer Endomondo accounts

* Go to /admin/config/services/endomondo and enter your Endomondo API key and secret.

-- CONTACT --

Current maintainers:
* Christopher von W�rden (CFE) - https://drupal.org/user/1284514

This project has been sponsored and developed by:
* b14
  Visit http://b14.dk for more information.