<?php
/**
 * @file
 * Integration layer to communicate with the Endomondo API 1.0.
 *
 * Original work by James Walker for Twitter module.
 * Modified by b14, Christopher von Würden (http://b14.dk).
 */

/**
 * Exception handling class.
 */
class EndomondoException extends Exception {}

/**
 * Primary Endomondo API implementation class
 */
class Endomondo {
  /**
   * @var $source the endomondo api 'source'
   */
  protected $source = 'drupal';

  protected $signature_method;

  protected $consumer;

  protected $token;


  /**
   * Authentication
   */
  /**
   * Authentication
   * Constructor for the Endomondo class
   */
  public function __construct($consumer_key, $consumer_secret, $oauth_token = NULL,
                              $oauth_token_secret = NULL) {
    $this->signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    $this->consumer = new OAuthConsumer($consumer_key, $consumer_secret);
    if (!empty($oauth_token) && !empty($oauth_token_secret)) {
      $this->token = new OAuthConsumer($oauth_token, $oauth_token_secret);
    }
  }

  public function get_request_token() {
    $url = variable_get('endomondo_api', ENDOMONDO_API) . '/oauth/request_token';
    try {
      $params = array('oauth_callback' => url('endomondo/oauth', array('absolute' => TRUE)));
      $response = $this->auth_request($url, $params);
    }
    catch (EndomondoException $e) {
      watchdog('endomondo', '!message', array('!message' => $e->__toString()), WATCHDOG_ERROR);
      return FALSE;
    }
    parse_str($response, $token);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  public function get_authorize_url($token) {
    $url = variable_get('endomondo_api', ENDOMONDO_HOST) . '/oauth/authorize';
    $url . = '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  public function get_authenticate_url($token) {
    $url = variable_get('endomondo_api', ENDOMONDO_API) . '/oauth/authenticate';
    $url . = '?oauth_token=' . $token['oauth_token'];

    return $url;
  }

  /**
   * Request an access token to the Endomondo API.
   *
   * @param string $oauth_verifier
   *   String an access token to append to the request or NULL.
   * @return
   *   String the access token or FALSE when there was an error.
   */
  public function get_access_token($oauth_verifier = NULL) {
    $url = variable_get('endomondo_api', ENDOMONDO_API) . '/oauth/access_token';

    // Adding parameter oauth_verifier to auth_request
    $parameters = array();
    if (!empty($oauth_verifier)) {
      $parameters['oauth_verifier'] = $oauth_verifier;
    }

    try {
      $response = $this->auth_request($url, $parameters);
    }
    catch (EndomondoException $e) {
      watchdog('endomondo', '!message', array('!message' => $e->__toString()), WATCHDOG_ERROR);
      return FALSE;
    }
    parse_str($response, $token);
    $this->token = new OAuthConsumer($token['oauth_token'], $token['oauth_token_secret']);
    return $token;
  }

  /**
   * Performs an authenticated request.
   */
  public function auth_request($url, $params = array(), $method = 'GET') {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->token, $method, $url, $params);
    $request->sign_request($this->signature_method, $this->consumer, $this->token);
    switch ($method) {
      case 'GET':
        return $this->request($request->to_url());
      case 'POST':
        return $this->request($request->get_normalized_http_url(), $request->get_parameters(), 'POST');
    }
  }

  /**
   * Performs a request.
   *
   * @throws EndomondoException
   */
  protected function request($url, $params = array(), $method = 'GET') {
    $data = '';
    if (count($params) > 0) {
      if ($method == 'GET') {
        $url . = '?' . http_build_query($params, '', '&');
      }
      else {
        $data = http_build_query($params, '', '&');
      }
    }

    $headers = array();

    $headers['Authorization'] = 'Oauth';
    $headers['Content-type'] = 'application/json';

    $response = $this->doRequest($url, $headers, $method, $data);

    if (!isset($response->error)) {
      return $response->data;
    }
    else {
      $error = $response->error;
      $data = $this->parse_response($response->data);
      if (isset($data['error'])) {
        $error = $data['error'];
      }
      throw new EndomondoException($error);
    }
  }

  /**
   * Actually performs a request.
   *
   * This method can be easily overriden through inheritance.
   *
   * @param string $url
   *   The url of the endpoint.
   * @param array $headers
   *   Array of headers.
   * @param string $method
   *   The HTTP method to use (normally POST or GET).
   * @param array $data
   *   An array of parameters
   * @return
   *   stdClass response object.
   */
  protected function doRequest($url, $headers, $method, $data) {
    return drupal_http_request($url, array('headers' => $headers, 'method' => $method, 'data' => $data));
  }

  protected function parse_response($response) {
    // http://drupal.org/node/985544 - json_decode large integer issue
    $length = strlen(PHP_INT_MAX);
    $response = preg_replace('/"(id|in_reply_to_status_id)":(\d{' . $length . ',})/', '"\1":"\2"', $response);
    return json_decode($response, TRUE);
  }
  /**
   * Creates an API endpoint URL.
   *
   * @param string $path
   *   The path of the endpoint.
   * @return
   *   The complete path to the endpoint.
   */
  protected function create_url($path) {
    $url =  variable_get('endomondo_api', ENDOMONDO_API) . '/api/1/' . $path;
    return $url;
  }

  /**
   * Returns a variety of information about the active user
   */
  public function users_show() {
    $params = array();
    $values = $this->call('user', $params, 'GET');
    return new EndomondoUser($values);
  }

  /**
   * Helpers used to convert responses in objects
   */
  /**
   * Get an array of EndomondoWorkout objects from an API endpoint
   */
  protected function get_workouts($path, $params = array()) {
    $values = $this->call($path, $params, 'GET');
    // Check on successful call

    if ($values) {
      $workouts = array();
      foreach ($values['data'] as $workout) {
        $workout['user'] = $params['account'];
        $workouts[] = new EndomondoWorkout($workout);
      }
      return $workouts;
    }
    // Call might return FALSE , e.g. on failed authentication
    else {
      // As call already throws an exception, we can return an empty array to
      // break no code.
      return array();
    }
  }

  /**
   * Get an array of EndomondoUser objects from an API endpoint
   */
  protected function get_users($path, $params = array()) {
    $values = $this->call($path, $params, 'GET');
    // Check on successful call
    if ($values) {
      $users = array();
      foreach ($values as $user) {
        $users[] = new EndomondoUser($user);
      }
      return $users;
    }
    // Call might return FALSE , e.g. on failed authentication
    else {
      // As call already throws an exception, we can return an empty array to
      // break no code.
      return array();
    }
  }

  /**
   * Workouts
   */
  /**
   * Returns the 10 most recent workouts for a user.
   *
   * @param array $params
   *   an array of parameters.
   */
  public function user_workouts($params = array()) {
    return $this->get_workouts('workouts', $params);
  }

  /**
   * Utilities
   */
  /**
   * Calls an Endomondo API endpoint.
   */
  public function call($path, $params = array(), $method = 'GET') {
    $url = $this->create_url($path);

    try {
      $response = $this->auth_request($url, $params, $method);
    }
    catch (EndomondoException $e) {
      watchdog('endomondo', '!message', array('!message' => $e->__toString()), WATCHDOG_ERROR);
      return FALSE;
    }

    if (!$response) {
      return FALSE;
    }

    return $this->parse_response($response);
  }
}

/**
 * Class for containing an individual Endomondo workout.
 */
class EndomondoWorkout {
  
  public $id;
  public $sport;
  public $source;
  public $start_time;
  public $end_time;
  public $title;
  public $distance_total;
  public $duration_total;
  public $calories_total;
  public $ascent_total;
  public $descent_total;
  public $steps_total;
  public $speed_max;
  public $altitude_min;
  public $altitude_max;
  public $heart_rate_avg;
  public $heart_rate_max;
  public $cadence_avg;
  public $cadence_max;
  public $user;

  /**
   * Constructor for EndomondoWorkout
   */
  public function __construct($values = array()) {
    $this->id = $values['id'];
    $this->sport = $values['sport'];
    $this->source = $values['source'];
    $this->start_time = $values['start_time'];
    $this->end_time = $values['end_time'];
    $this->title = isset($values['title'])?$values['title']:NULL;
    $this->distance_total = isset($values['distance_total'])?$values['distance_total']:0;
    $this->duration_total = isset($values['duration_total'])?$values['duration_total']:0;
    $this->calories_total = isset($values['calories_total'])?$values['calories_total']:0;
    $this->ascent_total = isset($values['ascent_total'])?$values['ascent_total']:0;
    $this->descent_total = isset($values['descent_total'])?$values['descent_total']:0;
    $this->steps_total = isset($values['steps_total'])?$values['steps_total']:0;
    $this->speed_max = isset($values['speed_max'])?$values['speed_max']:0;
    $this->altitude_min = isset($values['altitude_min'])?$values['altitude_min']:0;
    $this->altitude_max = isset($values['altitude_max'])?$values['altitude_max']:0;
    $this->heart_rate_avg = isset($values['heart_rate_avg'])?$values['heart_rate_avg']:0;
    $this->heart_rate_max = isset($values['heart_rate_max'])?$values['heart_rate_max']:0;
    $this->cadence_avg = isset($values['cadence_avg'])?$values['cadence_avg']:0;
    $this->cadence_max = isset($values['cadence_max'])?$values['cadence_max']:0;
    if (isset($values['user'])) {
      $this->user = $values['user'];
    }
  }
}

/**
 * Class for containing an Endomondo user.
 */
class EndomondoUser {

  public $id;
  public $name;
  public $first_name;
  public $last_name;
  public $gender;
  public $locale;
  public $timezone;
  protected $oauth_token;
  protected $oauth_token_secret;

  public function __construct($values = array()) {
    $this->id = $values['id'];
    $this->name = $values['name'];
    $this->first_name = $values['first_name'];
    $this->last_name = $values['last_name'];
    $this->gender = $values['gender'];
    $this->locale = $values['locale'];
    //$this->protected = $values['protected'];
    if (!empty($values['uid'])) {
      $this->uid = $values['uid'];
    }
    $this->timezone = $values['timezone']?$values['timezone']:0;
  }

  /**
   * Returns an array with the authentication tokens.
   *
   * @return
   *   array with the oauth token key and secret.
   */
  public function get_auth() {
    return array('oauth_token' => $this->oauth_token, 'oauth_token_secret' => $this->oauth_token_secret);
  }

  /**
   * Sets the authentication tokens to a user.
   *
   * @param array $values
   *   Array with 'oauth_token' and 'oauth_token_secret' keys.
   */
  public function set_auth($values) {
    $this->oauth_token = isset($values['oauth_token'])?$values['oauth_token']:NULL;
    $this->oauth_token_secret = isset($values['oauth_token_secret'])?$values['oauth_token_secret']:NULL;
  }

  /**
   * Checks whether the account is authenticated or not.
   *
   * @return
   *   boolean TRUE when the account is authenticated.
   */
  public function is_auth() {
    return !empty($this->oauth_token) && !empty($this->oauth_token_secret);
  }
}
