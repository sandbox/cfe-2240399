<?php
/**
 * @file
 * Theme file for the Endomondo stats block
 */
?>
<div class="stats">
<?php	foreach($workouts as $workout): ?>
	<div class="stat">
		<div class="stat-value">
			<?php
				$sport = t($workout->sport);
				
				$time = gmdate("H:i:s", $workout->duration_total);
				$date = strtotime($workout->start_time);
				$user = user_load($workout->user->uid);
				$profile = user_profile_load_by_user($user);
				
				$info = array(
					'@name' =>	$workout->user->name,
					'!link' => 'node/' . $profile->nid,
					'@sport' => $sport,
					'@distance' => round($workout->distance_total, 2) . ' ' . t('km'),
					'@time' => $time,
					'@date' => date('d.m.Y', $date),
				);
				print t('<a href="!link">@name</a> tracked @distance of @sport in @time. <i>@date</i>', $info);
			?>
		</div>
	</div>
<?php endforeach; ?>
</div>