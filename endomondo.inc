<?php

/**
 * @file
 * Endomondo API functions
 */

module_load_include('php', 'oauth_common', 'lib/OAuth');

/**
 * Connect to the Endomondo API.
 *
 * @param object $account
 *   An authenticated endomondo_account object to be used
 *   to authenticate against Endomondo.
 *
 * @return object
 *   a Endomondo object ready to be used to query the Endomondo API or FALSE.
 */
function endomondo_connect($account = NULL) {
  if (!$account) {
    // Load the first authenticated account.
    $endomondo_uid = db_query("SELECT endomondo_uid
                             FROM {endomondo_account}
                             WHERE oauth_token <> ''
                               AND oauth_token_secret <> '' ")->fetchField();
    $account = endomondo_account_load($endomondo_uid);
  }
  if ($account) {
    $auth = $account->get_auth();
    if (isset($auth['oauth_token']) && isset($auth['oauth_token_secret'])) {
      return new Endomondo(variable_get('endomondo_consumer_key', ''), variable_get('endomondo_consumer_secret', ''),
                              $auth['oauth_token'], $auth['oauth_token_secret']);
    }
  }
  return FALSE;
}

/**
 * Saves a EndomondoUser object to {endomondo_account}.
 */
function endomondo_account_save($endomondo_user, $save_auth = FALSE) {
  $values = (array) $endomondo_user;
  $values['endomondo_uid'] = $values['id'];
  foreach (array('protected', 'verified') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (int) $values[$k];
    }
  }

  if ($save_auth) {
    $values += $endomondo_user->get_auth();
  }
  $schema = drupal_get_schema('endomondo_account');
  foreach ($values as $k => $v) {
    if (!isset($schema['fields'][$k])) {
      unset($values[$k]);
    }
  }
  db_merge('endomondo_account')
    ->key(array('endomondo_uid' => $values['endomondo_uid']))
    ->fields($values)
    ->execute();

  // Notify other modules of the endomondo account save.
  module_invoke_all('endomondo_account_save', $values);
}

/**
 * Load a Endomondo account from {endomondo_account}.
 *
 * @param mixed $id
 *   int Endomondo User id or string Endomondo user screen name.
 *
 * @return object
 *   EndomondoUser object or NULL.
 */
function endomondo_account_load($id) {
  $values = db_query('SELECT *
                      FROM {endomondo_account}
                      WHERE endomondo_uid = :id_1
                      OR uid  = :id_2',
                      array(':id_1' => $id, ':id_2' => $id))
              ->fetchAssoc();
  if (!empty($values)) {
    $values['id'] = $values['endomondo_uid'];
    $account = new EndomondoUser($values);
    $account->set_auth($values);
    $account->import = $values['import'];
    return $account;
  }
  return NULL;
}

/**
 * Loads all Endomondo accounts added by a user.
 *
 * @return array
 *   array of EndomondoUser objects.
 */
function endomondo_account_load_all() {
  $accounts = array();
  $result = db_query('SELECT endomondo_uid
                      FROM {endomondo_account}
                      WHERE uid <> 0
                      ORDER BY name');
  foreach ($result as $account) {
    $accounts[] = endomondo_account_load($account->endomondo_uid);
  }
  return $accounts;
}

/**
 * Returns a list of authenticated Endomondo accounts.
 *
 * @return array
 *   array of EndomondoUser objects.
 */
function endomondo_load_authenticated_accounts() {
  $accounts = endomondo_account_load_all();
  $auth_accounts = array();
  foreach ($accounts as $account) {
    if ($account->is_auth()) {
      $auth_accounts[] = $account;
    }
  }
  return $auth_accounts;
}

/**
 * Load a Endomondo workout.
 *
 * @param int $workout_id
 *   The workout id of this workout.
 *
 * @return stdclass
 *   An instance of stdClass object with the workout data or FALSE.
 */
function endomondo_workout_load($workout_id) {
  return db_query("SELECT * FROM {endomondo} WHERE endomondo_id = :workout_id",
  array(':workout_id' => $workout_id))->fetchObject();
}

/**
 * Saves a EndomondoStatus object to {endomondo}.
 */
function endomondo_workout_save($workout) {
  $values = (array) $workout;

  $row = array(
    'endomondo_id' => $values['id'],
    'endomondo_uid' => $values['user']->id,
    'sport' => $values['sport'],
    'source' => $values['source'],
    'start_time' => $values['start_time'],
    'end_time' => $values['end_time'],
    'title' => $values['title'],
    'distance_total' => $values['distance_total'],
    'duration_total' => $values['duration_total'],
    'calories_total' => $values['calories_total'],
    'ascent_total' => $values['ascent_total'],
    'descent_total' => $values['descent_total'],
    'steps_total' => $values['steps_total'],
    'speed_max' => $values['speed_max'],
    'altitude_min' => $values['altitude_min'],
    'altitude_max' => $values['altitude_max'],
    'heart_rate_avg' => $values['heart_rate_avg'],
    'heart_rate_max' => $values['heart_rate_max'],
    'cadence_avg' => $values['cadence_avg'],
    'cadence_max' => $values['cadence_max'],
  );

  db_merge('endomondo')
    ->key(array('endomondo_id' => $row['endomondo_id']))
    ->fields($row)
    ->execute();
  // Let other modules know that a workout has been saved.
  module_invoke_all('endomondo_workout_save', $workout);
}

/**
 * Fetches a user's workout.
 */
function endomondo_fetch_workouts($id) {
  $account = endomondo_account_load($id);
  // Connect to the Endomondo's API.
  $endomondo = endomondo_connect($account);
  $params = array();
  $params['account'] = $account;

  // Fetch workout.
  $workouts = $endomondo->user_workouts($params);

  foreach ($workouts as $workout) {
    endomondo_workout_save($workout);
  }

  if (count($workouts) > 0) {
    // Update account details.
    endomondo_account_save($workouts[0]->user);
  }
}

/**
 * Gets workouts from database by user uid, endomondo user id or workout id.
 */
function endomondo_get_user_workouts($uid) {
  $account = endomondo_account_load($uid);
  $values = db_query('SELECT *
                      FROM {endomondo}
                      WHERE endomondo_uid = :uid',
                      array(':uid' => $account->id))
              ->fetchAllAssoc('endomondo_id');
  if (!empty($values)) {
    foreach ($values as $workout) {
      $workout = (array) $workout;
      $workout['id'] = $workout['endomondo_id'];
      $workout['user'] = $account;
      $workouts[] = new EndomondoWorkout($workout);
    }
    return $workouts;
  }
  return NULL;
}

/**
 * Gets all workouts from database.
 */
function endomondo_get_workouts($limit = 20) {
  $values = db_query("SELECT *
                      FROM {endomondo}
                      ORDER BY end_time DESC
                      LIMIT 0,$limit")
              ->fetchAllAssoc('endomondo_id');
  if (!empty($values)) {
    foreach ($values as $workout) {
      $workout = (array) $workout;
      $workout['id'] = $workout['endomondo_id'];
      $account = endomondo_account_load($workout['endomondo_uid']);
      $workout['user'] = $account;
      $workouts[] = new EndomondoWorkout($workout);
    }
    return $workouts;
  }
  return NULL;
}

/**
 * Delete a endomondo account and its workouts.
 *
 * @param int $endomondo_uid
 *   An integer with the Endomondo UID.
 */
function endomondo_account_delete($endomondo_uid) {
  // Delete from {endomondo_account}.
  $query = db_delete('endomondo_account');
  $query->condition('endomondo_uid', $endomondo_uid);
  $query->execute();

  // Delete from {endomondo}.
  $query = db_delete('endomondo');
  $query->condition('endomondo_uid', $endomondo_uid);
  $query->execute();

  // Delete from {authmap}.
  $query = db_delete('authmap');
  $query->condition('authname', $endomondo_uid);
  $query->condition('module', 'endomondo');
  $query->execute();
}

/**
 * Get sport human name by ID.
 */
function endomondo_get_sport($sportid = 1) {
  $sports = endomondo_get_sports();
  return $sports[$sportid];
}

/**
 * Helper function to convert sport IDs to text string, not currently in use.
 */
function endomondo_get_sports() {
  $sports = array(
    0 => 'running',
    1 => 'cycling transportation',
    2 => 'cycling sport',
    3 => 'mountain biking',
    4 => 'skating',
    5 => 'roller skiing',
    6 => 'skiing cross country',
    7 => 'skiing downhill',
    8 => 'snowboarding',
    9 => 'kayaking',
    10 => 'kite surfing',
    11 => 'rowing',
    12 => 'sailing',
    13 => 'windsurfing',
    14 => 'fitness walking',
    15 => 'golfing',
    16 => 'hiking',
    17 => 'orienteering',
    18 => 'walking',
    19 => 'riding',
    20 => 'swimming',
    21 => 'indoor cycling',
    22 => 'other',
    23 => 'aerobics',
    24 => 'badminton',
    25 => 'baseball',
    26 => 'basketball',
    27 => 'boxing',
    28 => 'climbing stairs',
    29 => 'cricket',
    30 => 'elliptical training',
    31 => 'dancing',
    32 => 'fencing',
    33 => 'amcerican football',
    34 => 'rugby',
    35 => 'soccer',
    36 => 'handball',
    37 => 'hockey',
    38 => 'pilates',
    39 => 'polo',
    40 => 'scuba diving',
    41 => 'squash',
    42 => 'table tennis',
    43 => 'tennis',
    44 => 'beach volley',
    45 => 'volleyball',
    46 => 'weight training',
    47 => 'yoga',
    48 => 'martial arts',
    49 => 'gymnastics',
    50 => 'step counter',
    87 => 'circuit Training',
    88 => 'treadmill running',
    89 => 'skateboarding',
    90 => 'surfing',
    91 => 'snowshoeing',
    92 => 'wheelchair',
    93 => 'climbing',
    94 => 'treadmill walking',
  );

  return $sports;
}
