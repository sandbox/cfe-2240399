<?php
/**
 * @file
 * Main module file of endomondo
 *
 * @TO-DO:
 * Implement overview of accounts.
 * Implement overview of user own workouts.
 * Delete endomondo account when associated Drupal account is deleted.
 */

define('ENDOMONDO_HOST', 'https://www.endomondo.com');
define('ENDOMONDO_API', 'https://api.endomondo.com');

/**
 * Implements hook_init().
 */
function endomondo_init() {
  $path = drupal_get_path('module', 'endomondo');
  drupal_add_css($path . '/css/endomondo.css');
}

/**
 * Implements hook_menu().
 */
function endomondo_menu() {
  $items = array();

  $items['endomondo/oauth'] = array(
    'title' => 'Endomondo OAuth',
    'access callback' => TRUE,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('endomondo_oauth_callback'),
    'type' => MENU_CALLBACK,
    'file' => 'endomondo.pages.inc',
  );

  $items['admin/config/services/endomondo'] = array(
    'title' => 'Endomondo',
    'description' => 'Endomondo accounts and settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('endomondo_admin_form'),
    'access arguments' => array('administer endomondo settings'),
    'file' => 'endomondo.pages.inc',
  );

  $items['user/%user/edit/endomondo'] = array(
    'title' => 'Endomondo accounts',
    'page callback' => 'endomondo_user_settings',
    'page arguments' => array(1),
    'access callback' => 'endomondo_account_access',
    'weight' => 10,
    'file' => 'endomondo.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function endomondo_permission() {
  return array(
    'administer endomondo settings' => array(
      'title' => t('Administer Endomondo settings'),
    ),
    'add authenticated endomondo accounts' => array(
      'title' => t('Add authenticated Endomondo accounts'),
    ),
    'administer endomondo accounts' => array(
      'title' => t('Administer Endomondo accounts'),
    ),
  );
}

/**
 * Access callback for the Endomondo accounts page.
 *
 * @return bool
 *   bool TRUE if the current user has access.
 */
function endomondo_account_access() {
  return user_access('add endomondo accounts') || user_access('add authenticated endomondo accounts');
}

/**
 * Implements hook_theme().
 */
function endomondo_theme() {
  return array(
    'endomondo_account_list_form' => array(
      'render element' => 'form',
    ),
    'endomondo_connect' => array(
      'render element' => 'form',
    ),
    'endomondo_user_stats' => array(
      'variables' => array('workouts' => array()),
      'template' => 'endomondo-user-stats',
    ),
    'endomondo_stats' => array(
      'variables' => array('workouts' => array()),
      'template' => 'endomondo-stats',
    ),
  );
}

/**
 * Implements hook_cron().
 *
 * Imports new Endomondo statuses for site users.
 */
function endomondo_cron() {
  if (!variable_get('endomondo_import', TRUE)) {
    return;
  }
  // Check if we can connect to Endomondo before proceeding.
  module_load_include('inc', 'endomondo');
  $endomondo = endomondo_connect();
  if (!$endomondo) {
    return;
  }

  // Pull up a list of Endomondo accounts that are flagged for updating,
  // sorted by how long it's been since we last updated them. This ensures
  // that the most out-of-date accounts get updated first.
  $result = db_query_range("SELECT endomondo_uid
                            FROM {endomondo_account}
                            WHERE uid <> 0 AND import = 1
                            ORDER BY last_refresh ASC",
                            0, 20);
  try {
    foreach ($result as $account) {
      // Fetch workouts.
      $endomondo_account = endomondo_account_load($account->endomondo_uid);
      if ($endomondo_account->is_auth() && variable_get('endomondo_import')) {
        endomondo_fetch_workouts($account->endomondo_uid);
      }
      // Mark the time this account was updated.
      db_update('endomondo_account')
        ->fields(array(
          'last_refresh' => REQUEST_TIME,
        ))
        ->condition('endomondo_uid', $account->endomondo_uid)
        ->execute();
    }
  }
  catch (EndomondoException $e) {
    // The exception has already been logged so we do not
    // need to do anything here apart from catching it.
  }
}

/**
 * Implements hook_block_info().
 */
function endomondo_block_info() {
  $blocks = array();
  $blocks['endomondo_connect'] = array(
    'info' => t('Endomondo connect'),
  );
  $blocks['endomondo_user_stats'] = array(
    'info' => t('Endomondo user stats'),
  );
  $blocks['endomondo_stats'] = array(
    'info' => t('Endomondo stats'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function endomondo_block_view($delta = '') {
  $block = array();
  global $user;
  $content = '';
  module_load_include('inc', 'endomondo');

  if ($delta == 'endomondo_connect') {
    if (!endomondo_api_keys()) {
      $variables = array('@endomondo-settings' => url('admin/config/services/endomondo/settings'));
      $content = '<p>' . t('You need to create an application in order to use the Endomondo API. Please fill out the OAuth fields at <a href="@endomondo-settings">Endomondo Settings</a> and then return here.', $variables) . '</p>';
    }
    else {
      module_load_include('inc', 'endomondo', 'endomondo.pages');

      if (user_is_logged_in() && !endomondo_account_load($user->uid)) {
        $content = drupal_get_form('endomondo_auth_account_form');
      }
      elseif (!user_is_logged_in()) {
        $content = t('You need to be logged in, in order to connect your profile with Endomondo.');
      }
      else {
        $content = t('Your account is connected with Endomondo.');
      }
    }
  }

  if ($delta == 'endomondo_user_stats') {
    $nid = arg(1);
    if (is_numeric($nid)) {
      $node = node_load($nid);
      $workouts = endomondo_get_user_workouts($node->uid);
      if (!count($workouts)) {
        return;
      }

      $totals = array(
        'distance_total' => array(
          'value' => 0,
          'title' => t('Distance total'),
          'unit' => t('km'),
        ),
        'duration_total' => array(
          'value' => 0,
          'title' => t('Duration total'),
          'unit' => t('seconds'),
        ),
        'calories_total' => array(
          'value' => 0,
          'title' => t('Calories total'),
          'unit' => t('kcal'),
        ),
        'ascent_total' => array(
          'value' => 0,
          'title' => t('Ascent total'),
          'unit' => t('m'),
        ),
        'descent_total' => array(
          'value' => 0,
          'title' => t('Descent total'),
          'unit' => t('m'),
        ),
        'steps_total' => array(
          'value' => 0,
          'title' => t('Steps total'),
          'unit' => '',
        ),
        'speed_max' => array(
          'value' => 0,
          'title' => t('Speed maximum'),
          'unit' => t('km/h'),
        ),
        'altitude_min' => array(
          'value' => $workouts[0]->altitude_min,
          'title' => t('Altitude minimum'),
          'unit' => t('m'),
        ),
        'altitude_max' => array(
          'value' => $workouts[0]->altitude_max,
          'title' => t('Altitude maximum'),
          'unit' => t('m'),
        ),
        'heart_rate_avg' => array(
          'value' => 0,
          'title' => t('Heart rate average'),
          'unit' => t('bpm'),
        ),
        'heart_rate_max' => array(
          'value' => 0,
          'title' => t('Heart rate maximum'),
          'unit' => t('bpm'),
        ),
        'cadence_avg' => array(
          'value' => 0,
          'title' => t('Cadence average'),
          'unit' => t('rpm'),
        ),
        'cadence_max' => array(
          'value' => 0,
          'title' => t('Cadence maximum'),
          'unit' => t('rpm'),
        ),
      );

      foreach ($workouts as $workout) {
        // Totals.
        $totals['distance_total']['value'] += $workout->distance_total;
        $totals['duration_total']['value'] += $workout->duration_total;
        $totals['calories_total']['value'] += $workout->calories_total;
        $totals['ascent_total']['value'] += $workout->ascent_total;
        $totals['descent_total']['value'] += $workout->descent_total;
        $totals['steps_total']['value'] += $workout->steps_total;

        // Max values.
        $totals['speed_max']['value'] = ($workout->speed_max > $totals['speed_max']['value']) ? $workout->speed_max : $totals['speed_max']['value'];
        $totals['altitude_max']['value'] = ($workout->altitude_max > $totals['altitude_max']['value']) ? $workout->altitude_max : $totals['altitude_max']['value'];
        $totals['heart_rate_max']['value'] = ($workout->heart_rate_max > $totals['heart_rate_max']['value']) ? $workout->heart_rate_max : $totals['heart_rate_max']['value'];
        $totals['cadence_max']['value'] = ($workout->cadence_max > $totals['cadence_max']['value']) ? $workout->cadence_max : $totals['cadence_max']['value'];

        // Min values.
        $totals['altitude_min']['value'] = ($workout->altitude_min < $totals['altitude_min']['value']) ? $workout->altitude_min : $totals['altitude_min']['value'];

        // Average values.
        $totals['cadence_avg']['value'] += $workout->cadence_avg;
        $totals['heart_rate_avg']['value'] += $workout->heart_rate_avg;
      }

      $totals['cadence_avg']['value'] = $totals['cadence_avg']['value'] / count($workouts);
      $totals['heart_rate_avg']['value'] = $totals['heart_rate_avg']['value'] / count($workouts);

      $settings['workouts'] = $totals;
    }
  }

  if ($delta == 'endomondo_stats') {
    $workouts = endomondo_get_workouts(7);
    $settings['workouts'] = $workouts;
  }

  switch ($delta) {
    case 'endomondo_connect':
      $block['subject'] = t('Connect with Endomondo');
      $block['content'] = $content;
      break;

    case 'endomondo_user_stats':
      $block['subject'] = t('Endomondo stats');
      $block['content'] = theme('endomondo_user_stats', $settings);
      break;

    case 'endomondo_stats':
      $block['subject'] = t('Endomondo');
      $block['content'] = theme('endomondo_stats', $settings);
      break;

  }
  return $block;
}

/**
 * Implements hook_node_view().
 *
 * Imports new Endomondo statuses for a profile when the profile node is viewed.
 */
function endomondo_node_view($node, $view_mode, $langcode) {
  if (!variable_get('endomondo_import', TRUE)) {
    return;
  }
  if ($node->type == 'profile' && $view_mode == 'full') {
    // Check if we can connect to Endomondo before proceeding.
    module_load_include('inc', 'endomondo');
    $endomondo = endomondo_connect();
    if (!$endomondo) {
      return;
    }

    // Pull up a list of Endomondo accounts that are flagged for updating,
    // sorted by how long it's been since we last updated them. This ensures
    // that the most out-of-date accounts get updated first.
    $result = db_query("SELECT endomondo_uid
                        FROM {endomondo_account}
                        WHERE uid = :uid AND import = 1",
                        array(':uid' => $node->uid));
    try {
      foreach ($result as $account) {
        // Fetch workouts.
        $endomondo_account = endomondo_account_load($account->endomondo_uid);
        if ($endomondo_account->is_auth() && variable_get('endomondo_import')) {
          endomondo_fetch_workouts($account->endomondo_uid);
        }
        // Mark the time this account was updated.
        db_update('endomondo_account')
          ->fields(array(
            'last_refresh' => REQUEST_TIME,
          ))
          ->condition('endomondo_uid', $account->endomondo_uid)
          ->execute();
      }
    }
    catch (EndomondoException $e) {
      // The exception has already been logged so we do
      // not need to do anything here apart from catching it.
    }
  }
}

/**
 * Implements hook_endomondo_accounts().
 */
function endomondo_endomondo_accounts($account) {
  module_load_include('inc', 'endomondo');

  $query = db_select('endomondo_account', 'ta')
    ->fields('ta', array('endomondo_uid'))
    ->condition('ta.uid', $account->uid);

  $endomondo_accounts = array();
  foreach ($query->execute()->fetchCol() as $endomondo_uid) {
    $endomondo_accounts[] = endomondo_account_load($endomondo_uid);
  }
  return $endomondo_accounts;
}

/**
 * Checks if the Endomondo Application keys are set.
 *
 * @return bool
 *   boolean TRUE if both the Endomondo Application key and secret are set.
 */
function endomondo_api_keys() {
  $key = variable_get('endomondo_consumer_key');
  $secret = variable_get('endomondo_consumer_secret');
  return !(empty($key) && empty($secret));
}